from collections import defaultdict

# import torch
# import torch.nn as nn
import tensorflow as tf
import time
import numpy as np


class SplitCrossEntropyLoss():
    r'''SplitCrossEntropyLoss calculates an approximate softmax'''
    def __init__(self, hidden_size, splits, verbose=False):
        # We assume splits is [0, split1, split2, N] where N >= |V|
        # For example, a vocab of 1000 words may have splits [0] + [100, 500] + [inf]
        self.hidden_size = hidden_size
        self.splits = [0] + splits + [100 * 1000000]
        self.nsplits = len(self.splits) - 1
        self.stats = defaultdict(list)
        self.verbose = verbose
        # Each of the splits that aren't in the head require a pretend token, we'll call them tombstones
        # The probability given to this tombstone is the probability of selecting an item from the represented split
        if self.nsplits > 1:
            self.tail_vectors = tf.get_variable('tail_vec',[self.nsplits - 1, hidden_size],tf.float32)
            self.tail_bias = tf.get_variable('tail_bias',[self.nsplits - 1],tf.float32)

    def logprob(self, weight, bias, hiddens, splits=None, softmaxed_head_res=None, verbose=False):
        # First we perform the first softmax on the head vocabulary and the tombstones
        if softmaxed_head_res is None:
            start, end = self.splits[0], self.splits[1]
            head_weight = None if end - start == 0 else weight[start:end]
            head_bias = None if end - start == 0 else bias[start:end]
            # We only add the tombstones if we have more than one split
            if self.nsplits > 1:
                head_weight = self.tail_vectors if head_weight is None else tf.concat([head_weight, self.tail_vectors],axis=0)
                head_bias = self.tail_bias if head_bias is None else tf.concat([head_bias, self.tail_bias],axis=0)

            # Perform the softmax calculation for the word vectors in the head for all splits
            # We need to guard against empty splits as torch.cat does not like random lists
            all_head_res = tf.matmul( head_weight,hiddens) +  head_bias
            softmaxed_head_res = tf.nn.log_softmax(all_head_res)

        if splits is None:
            splits = list(range(self.nsplits))

        results = []
        running_offset = 0
        for idx in splits:

            # For those targets in the head (idx == 0) we only need to return their loss
            if idx == 0:
                results.append(softmaxed_head_res[:, :-(self.nsplits - 1)])

            # If the target is in one of the splits, the probability is the p(tombstone) * p(word within tombstone)
            else:
                start, end = self.splits[idx], self.splits[idx + 1]
                tail_weight = weight[start:end]
                tail_bias = bias[start:end]
                # Calculate the softmax for the words in the tombstone
                tail_res = tf.matmul( hiddens, tf.transpose(tail_weight,[1,0])) +  tail_bias

                # Then we calculate p(tombstone) * p(word in tombstone)
                # Adding is equivalent to multiplication in log space
                head_entropy = softmaxed_head_res[:, -idx]
                tail_entropy = tf.nn.log_softmax(tail_res)
                results.append(tf.add(tf.reshape(head_entropy,[-1,1]) , tail_entropy))

        if len(results) > 1:
            return tf.stack(results, axis=1)
        return results[0]

    def split_on_targets(self, hiddens, targets):
        # Split the targets into those in the head and in the tail
        split_targets = []
        split_hiddens = []

        # Determine to which split each element belongs (for each start split value, add 1 if equal or greater)
        # This method appears slower at least for WT-103 values for approx softmax
        #masks = [(targets >= self.splits[idx]).view(1, -1) for idx in range(1, self.nsplits)]
        #mask = torch.sum(torch.cat(masks, dim=0), dim=0)
        ###
        # This is equally fast for smaller splits as method below but scales linearly
        mask = None
        for idx in range(1, self.nsplits):
            partial_mask = tf.cast(tf.greater_equal(targets, tf.constant(self.splits[idx])),tf.int32)
            mask = mask + partial_mask if mask is not None else partial_mask
        ###
        #masks = torch.stack([targets] * (self.nsplits - 1))
        #mask = torch.sum(masks >= self.split_starts, dim=0)
        done_words_count = tf.Variable(0,trainable=False)
        for idx in range(self.nsplits):
            # If there are no splits, avoid costly masked select
            if self.nsplits == 1:
                split_targets, split_hiddens = [targets], [hiddens]
                continue
            # If all the words are covered by earlier targets, we have empties so later stages don't freak out

            tmp_mask = tf.equal(mask, tf.constant(idx))
            done_words_count = done_words_count + tf.reduce_sum(tf.cast(tmp_mask,tf.int32))
            split_targets.append(tf.boolean_mask(targets, tmp_mask))
            tmp_mask_flattened = tf.reshape(tmp_mask,[-1])
            split_hiddens.append(tf.boolean_mask(hiddens,tmp_mask_flattened))

        return split_targets, split_hiddens

    def forward(self, weight, bias, hiddens, targets, verbose=False):
        if self.verbose or verbose:
            for idx in sorted(self.stats):
                print('{}: {}'.format(idx, int(np.mean(self.stats[idx]))))
            print()

        total_loss = None
        if len(hiddens.get_shape()) > 2: hiddens = tf.reshape(hiddens,[-1, hiddens.get_shape()[2]])

        split_targets, split_hiddens = self.split_on_targets(hiddens, targets)

        # First we perform the first softmax on the head vocabulary and the tombstones
        start, end = self.splits[0], self.splits[1]
        head_weight = None if end - start == 0 else weight[start:end]
        head_bias = None if end - start == 0 else bias[start:end]

        # We only add the tombstones if we have more than one split
        if self.nsplits > 1:
            head_weight = self.tail_vectors if head_weight is None else tf.concat([head_weight, self.tail_vectors],axis=0)
            head_bias = self.tail_bias if head_bias is None else tf.concat([head_bias, self.tail_bias],axis=0)

        # Perform the softmax calculation for the word vectors in the head for all splits
        # We need to guard against empty splits as torch.cat does not like random lists
        combo =tf.concat([split_hiddens[i] for i in range(self.nsplits) ],axis=0)
        ###
        all_head_res = tf.matmul( combo,tf.transpose(head_weight,[1,0])) +  head_bias
        softmaxed_all_head_res = tf.nn.log_softmax(all_head_res,axis=-1)
        if self.verbose or verbose:
            self.stats[0].append(combo.get_shape()[0] * head_weight.get_shape()[0])

        running_offset = tf.Variable(0,trainable=False)

        def custom_gather(tensor,indices):
          mask = tf.one_hot(indices, depth=tensor.get_shape().as_list()[-1], dtype=tf.bool, on_value=True, off_value=False)
          elements = -tf.boolean_mask(tensor, mask)
          return  elements

        def get_elements(data, indices):
          indeces = tf.range(0, tf.shape(indices)[0])*data.shape[1] + indices
          return -tf.gather(tf.reshape(data, [-1]), indeces)
        entropies = []
        for idx in range(self.nsplits):
            # If there are no targets for this split, continue

            # For those targets in the head (idx == 0) we only need to return their loss
            if idx == 0:
                # softmaxed_head_res = softmaxed_all_head_res[running_offset:running_offset + split_hiddens[idx].get_shape().as_list()[-2]]
                entropy = get_elements(softmaxed_all_head_res,tf.reshape(split_targets[idx],[-1]))
            # If the target is in one of the splits, the probability is the p(tombstone) * p(word within tombstone)
            else:
                softmaxed_head_res = tf.gather(softmaxed_all_head_res,axis=0,indices= tf.range(running_offset,running_offset + tf.shape(split_hiddens[idx])[0]))

                if self.verbose or verbose:
                    start, end = self.splits[idx], self.splits[idx + 1]
                    tail_weight = weight[start:end]
                    self.stats[idx].append(split_hiddens[idx].size()[0] * tail_weight.size()[0])

                # Calculate the softmax for the words in the tombstone
                tail_res = self.logprob(weight, bias, split_hiddens[idx], splits=[idx], softmaxed_head_res=softmaxed_head_res)

                # Then we calculate p(tombstone) * p(word in tombstone)
                # Adding is equivalent to multiplication in log space
                head_entropy = softmaxed_head_res[:, -idx]
                # All indices are shifted - if the first split handles [0,...,499] then the 500th in the second split will be 0 indexed
                indices = tf.reshape(split_targets[idx] - self.splits[idx],[-1,1])
                # Warning: if you don't squeeze, you get an N x 1 return, which acts oddly with broadcasting
                tail_entropy = get_elements(tf.nn.log_softmax(tail_res), tf.reshape(indices,[-1]))
                entropy = -(head_entropy + tail_entropy)
            ###
            running_offset += tf.shape(split_hiddens[idx])[0]
            entropies.append(tf.reduce_sum(entropy))
            total_loss = tf.reduce_sum(entropy) if total_loss is None else total_loss + tf.reduce_sum(entropy)

        return (total_loss,entropies)

if __name__ == '__main__':
    np.random.seed(42)

    V = 6000
    H = 10
    N = 1000
    E = 1000


    embed = tf.get_variable('embed',[V, H],tf.float32)
    crit = SplitCrossEntropyLoss(hidden_size=H, splits=[1000, 5000])
    bias = tf.get_variable('bias',[V],tf.float32)
    # optimizer = torch.optim.SGD(list(embed.parameters()) + list(crit.parameters()), lr=1)
    # prev =tf.Variable(np.random.randint(0,V,size=(1,N)))
    # x = tf.Variable(np.random.randint(0,V,size=(1,N)),dtype=tf.int32)
    prev =tf.Variable(np.random.geometric(p=0.00015, size=(10,N)))
    x = tf.Variable(np.random.geometric(p=0.00015, size=(10,N)),dtype=tf.int32)
    y = tf.nn.embedding_lookup(embed,prev)
    c = crit.forward(embed, bias, y, x)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        start = time.time()
        for _ in range(E):
            c_val = sess.run(c)
        print time.time() - start
        print c_val
            # print('Crit', c_val)

            # logprobs = crit.logprob(embed.weight, bias, y[:2]).exp()
            # print(logprobs)
            # print(logprobs.sum(dim=1))

            # optimizer.zero_grad()
            # c.backward()
            # optimizer.step()
