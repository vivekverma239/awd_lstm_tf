# from embed_regularize import embedded_dropout
# from locked_dropout import LockedDropout
# from weight_drop import WeightDrop
import tensorflow as tf

class RNNModel():
    """Container module with an encoder, a recurrent module, and a decoder."""

    def __init__(self, rnn_type, ntokens, ninp, nhid, nlayers, dropout=0.5, dropouth=0.5, dropouti=0.5, dropoute=0.1, wdrop=0, tie_weights=False):

        '''
        lockdrop: Variational dropout
        weight_drop: Drops weight of RNN

        '''

        self.rnn_type = rnn_type
        self.ninp = ninp
        self.nhid = nhid
        self.nlayers = nlayers
        self.ntokens= ntokens
        self.dropout = tf.Variable(dropout,trainable=False)
        self.dropouti = tf.Variable(dropouti,trainable=False)
        self.dropouth = tf.Variable(dropouth,trainable=False)
        self.dropoute = tf.Variable(dropoute,trainable=False)
        self.tie_weights = tie_weights
        assert rnn_type in ['LSTM', 'GRU'], 'RNN type is not supported'
        self.hidden_size = self.ninp if self.tie_weights else self.nhid
        self.rnns = tf.contrib.rnn.MultiRNNCell([self._get_lstm_cell(self.ninp if l == 0 else self.nhid, \
                                    self.nhid if l != self.nlayers - 1 else (self.ninp if self.tie_weights else self.nhid),\
                                    self.dropouth if l != self.nlayers -1 else self.dropout)\
                                    for l in range(self.nlayers)],state_is_tuple=True,\
                                     )

        # if wdrop:
        #     self.rnns = [WeightDrop(rnn, ['weight_hh_l0'], dropout=wdrop) for rnn in self.rnns]
        # elif rnn_type == 'QRNN':
        #     from torchqrnn import QRNNLayer
        #     self.rnns = [QRNNLayer(input_size=ninp if l == 0 else nhid, hidden_size=nhid if l != nlayers - 1 else (ninp if tie_weights else nhid), save_prev_x=True, zoneout=0, window=2 if l == 0 else 1, output_gate=True) for l in range(nlayers)]
        #     for rnn in self.rnns:
        #         rnn.linear = WeightDrop(rnn.linear, ['weight'], dropout=wdrop)




    def _get_lstm_cell(self, input_size, output_size,dropout=0):
        num_proj = None
        if input_size != output_size:
            num_proj =output_size

        if self.rnn_type == 'LSTM':
          cell = tf.contrib.rnn.LSTMCell(
              input_size, forget_bias=0.0, state_is_tuple=True,num_proj=num_proj)
        elif self.rnn_type == 'GRU':
          cell = tf.contrib.rnn.GRUCell(
              input_size, forget_bias=0.0,num_proj=num_proj)
        else:
            raise ValueError("rnn_mode %s not supported" % self.rnn_type)

        return tf.contrib.rnn.DropoutWrapper(cell, output_keep_prob = 1- dropout,\
                    variational_recurrent=True,dtype=tf.float32)


    def forward(self, input, num_steps,batch_size, return_h=False):

        self.encoder = tf.get_variable('embedding',[self.ntokens,self.ninp],dtype=tf.float32)


        # emb = embedded_dropout(self.encoder, input, keep_prob=1-self.dropoute\
        #                         if self.training else 0)
        emb = tf.nn.embedding_lookup(self.encoder,input)
        emb = tf.nn.dropout(emb, self.dropouti,noise_shape=[batch_size,1,self.ninp])

        raw_output = emb
        new_hidden = []
        raw_outputs = []
        outputs = []
        print(num_steps.get_shape())
        self.hidden_in = self.rnns.zero_state(tf.shape(input)[0],dtype=tf.float32)
        with tf.variable_scope("RNN"):
            outputs, hidden = tf.nn.dynamic_rnn(self.rnns, emb,
                                                 initial_state=self.hidden_in)


        with tf.device('/cpu:0'):
            if self.tie_weights:
                weight = tf.transpose(self.encoder,[1,0])
            else:
                weight = tf.get_variable('weight_final',[self.hidden_size,self.ntokens])
            bias = tf.get_variable('bias_final',[self.ntokens])
            outputs = tf.reshape(outputs,[-1,self.hidden_size])
            outputs = tf.nn.xw_plus_b(outputs,weight,bias)
        return outputs, hidden

    def get_dropout_vars(self):
        return (self.dropout,self.dropouth, self.dropouti, self.dropoute)
