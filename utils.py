

def batchify(data, bsz, args):
    # Work out how cleanly we can divide the dataset into bsz parts.
    nbatch = data.shape[0] // bsz
    # Trim off any extra elements that wouldn't cleanly fit (remainders).
    data = data[: nbatch * bsz]
    # Evenly divide the data across the bsz batches.
    data = data.reshape([bsz, -1])
    return data

def get_batch(source, i, args, seq_len=None, evaluation=False):
    seq_len = min(seq_len if seq_len else args.bptt, len(source) - 1 - i)
    data = source[:,i:i+seq_len]
    target = source[:,i+1:i+1+seq_len]
    return data, target
