import argparse
import time
import math
import numpy as np
import tensorflow as tf
import data
import model
import os
import hashlib

from utils import batchify, get_batch

parser = argparse.ArgumentParser(description='PyTorch PennTreeBank RNN/LSTM Language Model')
parser.add_argument('--data', type=str, default='data/penn/',
                    help='location of the data corpus')
parser.add_argument('--model', type=str, default='LSTM',
                    help='type of recurrent net (LSTM, QRNN, GRU)')
parser.add_argument('--emsize', type=int, default=400,
                    help='size of word embeddings')
parser.add_argument('--nhid', type=int, default=1150,
                    help='number of hidden units per layer')
parser.add_argument('--nlayers', type=int, default=3,
                    help='number of layers')
parser.add_argument('--lr', type=float, default=30,
                    help='initial learning rate')
parser.add_argument('--clip', type=float, default=0.25,
                    help='gradient clipping')
parser.add_argument('--epochs', type=int, default=8000,
                    help='upper epoch limit')
parser.add_argument('--batch_size', type=int, default=80, metavar='N',
                    help='batch size')
parser.add_argument('--eval_batch_size', type=int, default=80, metavar='N',
                    help='batch size')
parser.add_argument('--bptt', type=int, default=70,
                    help='sequence length')
parser.add_argument('--dropout', type=float, default=0.4,
                    help='dropout applied to layers (0 = no dropout)')
parser.add_argument('--dropouth', type=float, default=0.3,
                    help='dropout for rnn layers (0 = no dropout)')
parser.add_argument('--dropouti', type=float, default=0.65,
                    help='dropout for input embedding layers (0 = no dropout)')
parser.add_argument('--dropoute', type=float, default=0.1,
                    help='dropout to remove words from embedding layer (0 = no dropout)')
parser.add_argument('--wdrop', type=float, default=0.5,
                    help='amount of weight dropout to apply to the RNN hidden to hidden matrix')
parser.add_argument('--seed', type=int, default=1111,
                    help='random seed')
parser.add_argument('--nonmono', type=int, default=5,
                    help='random seed')
parser.add_argument('--cuda', action='store_false',
                    help='use CUDA')
parser.add_argument('--log-interval', type=int, default=2, metavar='N',
                    help='report interval')
randomhash = ''.join(str(time.time()).split('.'))
parser.add_argument('--save', type=str,  default=randomhash+'.pt',
                    help='path to save the final model')
parser.add_argument('--alpha', type=float, default=2,
                    help='alpha L2 regularization on RNN activation (alpha = 0 means no regularization)')
parser.add_argument('--beta', type=float, default=1,
                    help='beta slowness regularization applied on RNN activiation (beta = 0 means no regularization)')
parser.add_argument('--wdecay', type=float, default=1.2e-6,
                    help='weight decay applied to all weights')
parser.add_argument('--resume', type=str,  default='',
                    help='path of model to resume')
parser.add_argument('--optimizer', type=str,  default='sgd',
                    help='optimizer to use (sgd, adam)')
parser.add_argument('--when', nargs="+", type=int, default=[-1],
                    help='When (which epochs) to divide the learning rate by 10 - accepts multiple')
args = parser.parse_args()
args.tied = True

# Set the random seed manually for reproducibility.
np.random.seed(args.seed)


###############################################################################
# Load data
###############################################################################

def model_save(fn,sess):
    '''
    Change this with tensorflow saver
    '''
    with open(fn, 'wb') as f:
        torch.save([model, criterion, optimizer], f)

def model_load(fn,sess):
    '''
    Change this with tensorflow saver
    '''
    global model, criterion, optimizer
    with open(fn, 'rb') as f:
        model, criterion, optimizer = torch.load(f)

import pickle
fn = 'corpus.{}.data'.format(hashlib.md5(args.data.encode()).hexdigest())
if os.path.exists(fn):
    print('Loading cached dataset...')
    corpus = pickle.load(open(fn))
else:
    print('Producing dataset...')
    corpus = data.Corpus(args.data)
    pickle.dump(corpus, open(fn,'w'))

eval_batch_size = args.eval_batch_size
test_batch_size = 1
train_data = batchify(corpus.train, args.batch_size, args)
val_data = batchify(corpus.valid, eval_batch_size, args)
test_data = batchify(corpus.test, test_batch_size, args)

print('Train Data Shape ', train_data.shape)
print('Val Data Shape ', val_data.shape)
print('Test Data Shape ', test_data.shape)
###############################################################################
# Build the model
###############################################################################

from splitcross import SplitCrossEntropyLoss
criterion = None

ntokens = len(corpus.dictionary)
###
# if args.resume:
#     print('Resuming model ...')
#     model_load(args.resume)
#     optimizer.param_groups[0]['lr'] = args.lr
#     model.dropouti, model.dropouth, model.dropout, args.dropoute = args.dropouti, args.dropouth, args.dropout, args.dropoute
#     if args.wdrop:
#         from weight_drop import WeightDrop
#         for rnn in model.rnns:
#             if type(rnn) == WeightDrop: rnn.dropout = args.wdrop
#             elif rnn.zoneout > 0: rnn.zoneout = args.wdrop
###
# if not criterion:
#     splits = []
#     if ntokens > 500000:
#         # One Billion
#         # This produces fairly even matrix mults for the buckets:
#         # 0: 11723136, 1: 10854630, 2: 11270961, 3: 11219422
#         splits = [4200, 35000, 180000]
#     elif ntokens > 75000:
#         # WikiText-103
#         splits = [2800, 20000, 76000]
#     print('Using', splits)
#     criterion = SplitCrossEntropyLoss(args.emsize, splits=splits, verbose=False)
###

###
print('Args:', args)

###############################################################################
# Training code
###############################################################################

def evaluate(sess,data_source, batch_size=10):
    # Turn on evaluation mode which disables dropout.
    model.eval()
    total_loss = 0
    ntokens = len(corpus.dictionary)
    # hidden = model.init_hidden(batch_size)
    for i in range(0, data_source.size(0) - 1, args.bptt):
        data, targets = get_batch(data_source, i, args, evaluation=True)
        output, hidden = model(data, hidden)
        total_loss += len(data) * criterion(model.decoder.weight, model.decoder.bias, output, targets).data
    return total_loss[0] / len(data_source)

class helper():
    def __init__(self,sess,train_data,test_data,args):
        self.args = args
        self.batch_size  = 64
        self.ntokens = len(corpus.dictionary)
        self.sess = sess
        self.model_instance = model.RNNModel(args.model, ntokens, args.emsize, args.nhid,\
                                args.nlayers, args.dropout, args.dropouth,\
                                args.dropouti, args.dropoute, args.wdrop,\
                                args.tied)
        self.dropout,self.dropouth, self.dropouti, self.dropoute = self.model_instance.get_dropout_vars()

        self.inputs = tf.placeholder(tf.int32,[None,None],'inputs')
        self.targets = tf.placeholder(tf.int32,[None,None], 'targets')
        self.bptt_var = tf.Variable(self.args.bptt,trainable=False)
        self.batch_size_var = tf.Variable(self.batch_size,trainable=False)
        self.lr_var = tf.Variable(self.args.lr,trainable=False,dtype=tf.float32)
        self.lr_var_placeholder = tf.placeholder(tf.float32,[],'lr_var_placeholder')
        self.batch_size_var_placeholder = tf.placeholder(tf.int32,[],'batch_size_var_placeholder')
        self.bptt_var_placeholder =tf.placeholder(tf.int32,[],'bptt_var_placeholder')
        self.lr_var_update = tf.assign(self.lr_var,self.lr_var_placeholder)
        self.batch_size_var_update =tf.assign(self.batch_size_var,self.batch_size_var_placeholder)
        self.bptt_var_update =tf.assign(self.bptt_var,self.bptt_var_placeholder)
        self.outputs, self.hidden_out = self.model_instance.forward(self.inputs,self.bptt_var,self.batch_size_var )
        self.get_train_op()
        self.initial_state = self.model_instance.hidden_in

    def save_model(self):
        pass

    def load_model(self):
        pass

    def get_train_op(self):
        self.loss= tf.reduce_sum(tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.outputs,labels=tf.reshape(self.targets,[-1]))))
        if self.args.optimizer == 'sgd':
            optimizer = tf.train.GradientDescentOptimizer(learning_rate=self.lr_var)
        if self.args.optimizer == 'adam':
            optimizer = tf.train.AdamOptimizer(learning_rate=self.lr_var)
        tvars = tf.trainable_variables()
        grads, _ = tf.clip_by_global_norm(tf.gradients(self.loss, tvars),
                                          self.args.clip)
        self._train_op = optimizer.apply_gradients(
            zip(grads, tvars),
            )

    def run_assign_ops(self,lr=None,batch_size=None,bptt=None):
        feed_dict ={}
        ops = []
        if lr:
            feed_dict[self.lr_var_placeholder] = lr
            ops.append(self.lr_var_update)
        if batch_size:
            feed_dict[self.batch_size_var_placeholder] = batch_size
            ops.append(self.batch_size_var_update)
        if bptt:
            feed_dict[self.bptt_var_placeholder] = bptt
            ops.append(self.bptt_var_update)
        if ops:
            self.sess.run(ops,feed_dict)


    def run_epoch(self,epoch=0):
        total_loss = 0
        epoch = 0
        start_time = time.time()
        batch, i = 0, 0
        self.run_assign_ops(batch_size=self.args.batch_size)
        while i < train_data.shape[0] - 1 - 1:
            feed_dict = {}
            bptt = self.args.bptt if np.random.random() < 0.95 else self.args.bptt / 2.
            # Prevent excessively small or negative sequence lengths
            seq_len = max(5, int(np.random.normal(bptt, 5)))
            self.run_assign_ops(bptt=seq_len)
            data, targets = get_batch(train_data, i, args, seq_len=seq_len)
            if i ==0 :
                state = self.sess.run(self.initial_state,{self.inputs:data,self.targets:targets})

            for i, (c, h) in enumerate(self.initial_state):
                feed_dict[c] = state[i].c
                feed_dict[h] = state[i].h
            feed_dict[self.inputs]  = data
            feed_dict[self.targets] = targets

            _, loss, state = self.sess.run([self._train_op,self.loss,self.hidden_out],feed_dict)

            total_loss += loss
            if batch % self.args.log_interval == 0 and batch > 0:
                cur_loss = total_loss / (self.args.log_interval*i)
                elapsed = time.time() - start_time
                print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:05.5f} | ms/batch {:5.2f} | '
                        'loss {:5.2f} | ppl {:8.2f} | bpc {:8.3f}'.format(
                    epoch, batch, len(train_data) // self.args.bptt, self.args.lr,
                    elapsed * 1000 / args.log_interval, cur_loss, math.exp(cur_loss), cur_loss / math.log(2)))
                total_loss = 0
                start_time = time.time()

            batch += 1
            i += seq_len

        return None

    def train(self):
            self.sess.run(tf.global_variables_initializer())
            lr = args.lr
            best_val_loss = []
            stored_loss = 100000000
            for epoch in range(1, args.epochs+1):
                epoch_start_time = time.time()
                self.run_epoch(epoch)
                # val_loss = evaluate(val_data, eval_batch_size)
                # print('-' * 89)
                # print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | '
                #     'valid ppl {:8.2f} | valid bpc {:8.3f}'.format(
                #   epoch, (time.time() - epoch_start_time), val_loss, math.exp(val_loss), val_loss / math.log(2)))
                # print('-' * 89)
                #
                # if val_loss < stored_loss:
                #     model_save(self.args.save)
                #     print('Saving model (new best validation)')
                #     stored_loss = val_loss
                #
                # # if args.optimizer == 'sgd' and 't0' not in optimizer.param_groups[0] and (len(best_val_loss)>args.nonmono and val_loss > min(best_val_loss[:-args.nonmono])):
                # #     print('Switching to ASGD')
                # #     optimizer = torch.optim.ASGD(model.parameters(), lr=args.lr, t0=0, lambd=0., weight_decay=args.wdecay)
                #
                if epoch in args.when:
                    print('Saving model before learning rate decreased')
                    # model_save('{}.e{}'.format(self.args.save, epoch))
                    print('Dividing learning rate by 10')
                    self.args.lr /= 10.
                    self.run_assign_ops(lr=self.args.lr)
                #
                # best_val_loss.append(val_loss)

    def evaluate():
        pass

sess = tf.Session()
train_helper = helper(sess,train_data,val_data,args)
train_helper.train()
